"""
A light layer to use OWFS and pyownet with a more OOP approach.
"""
from owclient.owclient import OwClient

__version__ = '0.1.8'
__all__ = ['OwClient']
