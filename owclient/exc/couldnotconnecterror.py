class CouldNotConnectError(Exception):
    """
    Exception raised when proxy fails to connect to owserver
    """
    pass
