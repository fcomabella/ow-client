from typing import Any
from typing import Dict
from typing import List
from typing import Tuple
from typing import Type

import pyownet
import pytest

from owclient import OwClient


@pytest.fixture
def mock_proxy():
    class _Proxy:
        """
        A pyownet._Proxy mock class
        """
        def __init__(self, host: str = 'localhost', port: int = 4304) -> None:
            if proxy_config.raise_on_connect:
                raise pyownet.protocol.ConnError()

        def __get_device(self, path: str) -> Dict:
            return next(d for d in proxy_config.devices if d['path'] == path)

        def __clean_path(self, path: str) -> str:
            if path != '/':
                if path[0] == '/':
                    path = path[1:]

                if path[-1] == '/':
                    path = path[:-1]

            return path

        def __get_path_and_property(self, path: str) -> Tuple[str, str]:
            path = self.__clean_path(path)
            split_path = path.split('/')

            if 'uncached' in split_path:
                device_path = split_path[1]
            else:
                device_path = split_path[0]

            prop = split_path[-1]

            return device_path, prop

        def read(self, full_path: str) -> bytes:
            if proxy_config.raise_on_read:
                raise pyownet.protocol.Error()

            (path, prop) = self.__get_path_and_property(full_path)
            try:
                device = self.__get_device(path)
                if prop.startswith('r_'):
                    return device[prop[2:]][::-1]
                else:
                    return device[prop]
            except StopIteration as ex:
                raise pyownet.protocol.OwnetError() from ex
            except AttributeError as ex:
                raise pyownet.protocol.Error() from ex

        def write(self, full_path: str, value: bytes) -> None:
            if proxy_config.raise_on_write:
                raise pyownet.protocol.Error()

            (path, prop) = self.__get_path_and_property(full_path)
            try:
                device = self.__get_device(path)
                device[prop] = value
            except StopIteration as ex:
                raise pyownet.protocol.OwnetError() from ex
            except AttributeError as ex:
                raise pyownet.protocol.Error() from ex

        def dir(self, path: str, slash: bool = True) -> List[str]:
            if proxy_config.raise_on_dir:
                raise pyownet.protocol.Error

            path = self.__clean_path(path)

            if slash is True:
                return [
                    f'/{device["path"]}/' for device in proxy_config.devices
                ]
            else:
                return [
                    f'/{device["path"]}' for device in proxy_config.devices
                ]

    class OneWire:
        def __init__(self):
            self.devices = []
            self.raise_on_connect = False
            self.raise_on_read = False
            self.raise_on_write = False
            self.raise_on_dir = False

    proxy_config = OneWire()

    return proxy_config, _Proxy


@pytest.fixture
def patched_owclient(mock_proxy, monkeypatch) -> Tuple[Any, Type[OwClient]]:
    (proxy_config, _Proxy) = mock_proxy
    monkeypatch.setattr(pyownet.protocol, 'proxy', _Proxy)

    return proxy_config, OwClient
