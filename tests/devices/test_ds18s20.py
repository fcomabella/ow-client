import pytest

from owclient.devices import DS18S20, DS1920


@pytest.fixture
def mock_ds18s20(mock_proxy):
    (proxy_config, _Proxy) = mock_proxy
    base_dict = {
        'path': 'path',
        'address': b'test-address',
        'crc8': b'test-crc8',
        'family': b'test-family',
        'id': b'test-id',
        'locator': b'test-locator',
        'temperature': b'45.5',
        'temphigh': b'40',
        'templow': b'0'
    }
    proxy_config.devices = [base_dict]

    def make_device(path: str = '/path/', uncached: bool = True) -> DS18S20:
        proxy = _Proxy()
        return DS18S20(proxy, path, uncached)

    return base_dict, make_device


@pytest.fixture
def mock_ds1920(mock_proxy):
    (proxy_config, proxy) = mock_proxy
    base_dict = {
        'path': 'path',
        'address': b'test-address',
        'crc8': b'test-crc8',
        'family': b'test-family',
        'id': b'test-id',
        'locator': b'test-locator',
        'temperature': b'45.5',
        'temphigh': b'40',
        'templow': b'0'
    }
    proxy_config.devices = [base_dict]

    def make_device(path: str = '/path/', uncached: bool = True) -> DS18S20:
        return DS1920(proxy, path, uncached)

    return base_dict, make_device


def test_temphigh_read(mock_ds18s20):
    (base_dict, make_device) = mock_ds18s20
    device = make_device()
    assert device.temphigh == int(base_dict['temphigh'])


def test_temphigh_write(mock_ds18s20):
    (base_dict, make_device) = mock_ds18s20
    device = make_device()
    new_temphigh = 80
    device.temphigh = new_temphigh
    assert int(float(base_dict['temphigh'])) == new_temphigh


def test_templow_read(mock_ds18s20):
    (base_dict, make_device) = mock_ds18s20
    device = make_device()
    assert device.templow == int(base_dict['templow'])


def test_templow_write(mock_ds18s20):
    (base_dict, make_device) = mock_ds18s20
    device = make_device()
    new_templow = 10
    device.templow = new_templow
    assert int(float(base_dict['templow'])) == new_templow


def test_ds1920(mock_ds1920):
    (base_dict, make_device) = mock_ds1920
    device = make_device()
    assert issubclass(type(device), DS18S20)
