import pytest

from owclient.devices import Thermometer


@pytest.fixture
def mock_thermometer(mock_proxy):
    (proxy_config, _Proxy) = mock_proxy
    base_dict = {
        'path': 'path',
        'address': b'test-address',
        'crc8': b'test-crc8',
        'family': b'test-family',
        'id': b'test-id',
        'locator': b'test-locator',
        'temperature': b'45'
    }
    proxy_config.devices = [base_dict]

    def make_device(path: str = '/path/',
                    uncached: bool = True) -> Thermometer:
        proxy = _Proxy()
        return Thermometer(proxy, path, uncached)

    return base_dict, make_device


def test_temperature(mock_thermometer):
    (base_dict, make_device) = mock_thermometer
    device = make_device()

    assert device.temperature == float(base_dict['temperature'])
