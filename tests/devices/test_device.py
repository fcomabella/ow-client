from typing import Callable
from typing import Dict
from typing import Tuple

import pytest

from owclient.devices import Device
from owclient.exc import CouldNotReadError
from owclient.exc import CouldNotWriteError


@pytest.fixture
def mock_device(mock_proxy) -> Tuple[Dict[str, object], Callable[..., Device]]:
    (proxy_config, _Proxy) = mock_proxy

    base_dict = {
        'path': 'path',
        'address': b'test-address',
        'crc8': b'test-crc8',
        'family': b'test-family',
        'id': b'test-id',
        'locator': b'test-locator'
    }

    proxy_config.devices = [base_dict]

    def make_device(path: str = '/path/', uncached: bool = True) -> Device:
        proxy = _Proxy()
        return Device(proxy, path, uncached)

    return base_dict, make_device


def test_constructor_defaults(
        mock_device: Tuple[Dict[str, object], Callable[..., Device]]) -> None:
    (_, make_device) = mock_device
    path = '/test_path/'
    device = make_device(path)

    assert device.uncached is True, 'device.uncached should be True'
    assert device.path == path


def test_constructor_cached(mock_device):
    (_, make_device) = mock_device
    path = '/test_path/'
    device = make_device(path, False)

    assert device.uncached is False, 'device.uncached should be False'


def test_uncached_repr(mock_device):
    (base_dict, make_device) = mock_device
    device = make_device()
    assert repr(device) == (f'{type(device).__name__}('
                            f'proxy={device._proxy}, '
                            f'path={device.path}, '
                            f'uncached={device.uncached}'
                            ')')


def test_read_requires_property(mock_device):
    (base_dict, make_device) = mock_device
    device = make_device()

    with pytest.raises(TypeError) as exc:
        device._read()

    assert 'property' in str(exc), \
        'Device _read should require property argument'


def test_read_raises_could_not_read_error(mock_proxy, mock_device):
    (proxy_config, proxy) = mock_proxy
    (base_dict, make_device) = mock_device
    device = make_device()
    proxy_config.raise_on_read = True

    with pytest.raises(CouldNotReadError):
        device._read('address')


def test_read(mock_device):
    (base_dict, make_device) = mock_device
    device = make_device()
    value = device._read('address')

    assert isinstance(value, bytes), \
        'return value must be a bytes type'
    assert value is base_dict['address'], \
        'return value should be "address" property value'


def test_write_requires_property_value(mock_device):
    (base_dict, make_device) = mock_device
    device = make_device()

    with pytest.raises(TypeError) as exc:
        device._write()

    assert 'property' in str(exc.value), \
        'Device _write should require property argument'
    assert 'value' in str(exc.value), \
        'Device _write should require value argument'


def test_write_raises_could_not_write_error(mock_proxy, mock_device):
    (proxy_config, proxy) = mock_proxy
    (base_dict, make_device) = mock_device
    device = make_device()
    proxy_config.raise_on_write = True

    with pytest.raises(CouldNotWriteError):
        device._write('address', b'new-value')


def test_write(mock_device, mocker):
    (base_dict, make_device) = mock_device
    device = make_device()
    value = b'new-value'
    write_spy = mocker.spy(device._proxy, 'write')

    device._write('address', value)

    write_spy.assert_called_with(f'{device.query_path}address', value)


def test_uncached_query_path(mock_device):
    (base_dict, make_device) = mock_device
    device = make_device()
    assert device.query_path == f'uncached/path/'


def test_cached_query_path(mock_device):
    (base_dict, make_device) = mock_device
    device = make_device(uncached=False)
    assert device.query_path == '/path/'


def test_address(mock_device):
    (base_dict, make_device) = mock_device
    device = make_device()

    assert device.address == base_dict['address'].decode('ascii')


def test_r_address(mock_device):
    (base_dict, make_device) = mock_device
    device = make_device()

    assert device.r_address == base_dict['address'][::-1].decode('ascii')


def test_crc8(mock_device):
    (base_dict, make_device) = mock_device
    device = make_device()

    assert device.crc8 == base_dict['crc8'].decode('ascii')


def test_family(mock_device):
    (base_dict, make_device) = mock_device
    device = make_device()

    assert device.family == base_dict['family'].decode('ascii')


def test_id(mock_device):
    (base_dict, make_device) = mock_device
    device = make_device()

    assert device.id == base_dict['id'].decode('ascii')


def test_r_id(mock_device):
    (base_dict, make_device) = mock_device
    device = make_device()

    assert device.r_id == base_dict['id'][::-1].decode('ascii')


def test_locator(mock_device):
    (base_dict, make_device) = mock_device
    device = make_device()

    assert device.locator == base_dict['locator'].decode('ascii')


def test_r_locator(mock_device):
    (base_dict, make_device) = mock_device
    device = make_device()
    assert device.r_locator == base_dict['locator'][::-1].decode('ascii')
