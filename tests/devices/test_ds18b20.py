from typing import Callable
from typing import Dict
from typing import Tuple

import pytest

from owclient.devices import DS18B20


@pytest.fixture
def mock_ds18b20(
        mock_proxy) -> Tuple[Dict[str, object], Callable[..., DS18B20]]:
    (proxy_config, _Proxy) = mock_proxy

    base_dict = {
        'path': 'path',
        'address': b'test-address',
        'crc8': b'test-crc8',
        'family': b'test-family',
        'id': b'test-id',
        'locator': b'test-locator',
        'temperature': b'12',
        'temphigh': b'40',
        'templow': b'0',
        'power': b'0',
        'temperature9': b'9',
        'temperature10': b'10',
        'temperature11': b'11',
        'temperature12': b'12',
        'fasttemp': b'9'
    }

    proxy_config.devices = [base_dict]

    def make_device(path: str = '/path/', uncached: bool = True) -> DS18B20:
        proxy = _Proxy()
        return DS18B20(proxy, path, uncached)

    return base_dict, make_device


def test_power(mock_ds18b20):
    (base_dict, make_device) = mock_ds18b20
    device = make_device()
    assert device.power == bool(base_dict['power'])

    base_dict['power'] = b'1'
    assert device.power == bool(base_dict['power'])


@pytest.mark.parametrize('precision', range(9, 13))
def test_temperature_with_precision(mock_ds18b20, precision):
    (base_dict, make_device) = mock_ds18b20
    device = make_device()
    read = getattr(device, f'temperature{precision}')
    expected = float(base_dict.get(f'temperature{precision}'))

    assert read == expected


def test_fasttemp(mock_ds18b20):
    (base_dict, make_device) = mock_ds18b20
    device = make_device()

    assert device.fasttemp == float(base_dict['fasttemp'])


def test_temphigh_read(mock_ds18b20):
    (base_dict, make_device) = mock_ds18b20
    device = make_device()

    assert device.temphigh == int(base_dict['temphigh'])


def test_temphigh_write(mock_ds18b20):
    (base_dict, make_device) = mock_ds18b20
    device = make_device()
    new_temphigh = 80
    device.temphigh = new_temphigh

    assert int(base_dict['temphigh']) == new_temphigh


def test_templow_read(mock_ds18b20):
    (base_dict, make_device) = mock_ds18b20
    device = make_device()

    assert device.templow == int(base_dict['templow'])


def test_templow_write(mock_ds18b20):
    (base_dict, make_device) = mock_ds18b20
    device = make_device()
    new_templow = 80
    device.templow = new_templow
    assert int(base_dict['templow']) == new_templow
