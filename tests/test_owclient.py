import logging

import pytest

from owclient import __version__
from owclient.devices import DS18B20
from owclient.devices import DS18S20
from owclient.devices import Device
from owclient.exc import CouldNotConnectError
from owclient.exc import CouldNotLoadDeviceError
from owclient.exc import CouldNotLoadDevicesError
from owclient.exc import UnrecognizedDeviceError


def test_version():
    assert __version__ == '0.1.8'


def test_constructor(patched_owclient):
    (proxy_config, OwClient) = patched_owclient

    owc = OwClient()
    assert owc.uncached is True
    assert owc.host == 'localhost'
    assert owc.port == 4304
    assert isinstance(owc.logger, logging.Logger)

    owc = OwClient('test_host', 1234, False)
    assert owc.uncached is False
    assert owc.host == 'test_host'
    assert owc.port == 1234


def test_repr(patched_owclient):
    (proxy_config, OwClient) = patched_owclient

    owc = OwClient()

    assert repr(owc) == (f'{type(owc).__name__}('
                         f'host={owc.host}, '
                         f'port={owc.port}, '
                         f'uncached={owc.uncached} '
                         f')')


def test_load_device_connection_error_raises(patched_owclient):
    (proxy_config, OwClient) = patched_owclient
    proxy_config.raise_on_connect = True
    owc = OwClient()

    with pytest.raises(CouldNotConnectError):
        owc.load_device('/device1/')


def test_load_device_without_devices_raises(patched_owclient):
    (proxy_config, OwClient) = patched_owclient

    owc = OwClient()

    with pytest.raises(CouldNotLoadDeviceError):
        owc.load_device('/device1/')


def test_load_unenxisting_device_raises(patched_owclient):
    (proxy_config, OwClient) = patched_owclient
    proxy_config.devices = [{'type': b'DS18B20', 'path': 'existing'}]
    owc = OwClient()

    with pytest.raises(CouldNotLoadDeviceError):
        owc.load_device('/nonexisting/')


def test_load_device_unrecognized_raises(patched_owclient):
    (proxy_config, OwClient) = patched_owclient
    proxy_config.devices = [{'type': b'ANOTHER', 'path': 'existing'}]
    owc = OwClient()

    with pytest.raises(UnrecognizedDeviceError):
        owc.load_device('/existing/', True)


def test_load_device_unrecognized_returns_device(patched_owclient):
    (proxy_config, OwClient) = patched_owclient
    proxy_config.devices = [{'type': b'ANOTHER', 'path': 'existing'}]
    owc = OwClient()
    device = owc.load_device('existing/')

    assert isinstance(device, Device)


def test_load_device_returns_device_type_instance(patched_owclient):
    (proxy_config, OwClient) = patched_owclient
    proxy_config.devices = [{'type': b'DS18B20', 'path': 'DS18B20'}]
    owc = OwClient()

    device = owc.load_device('/DS18B20/')
    assert isinstance(device, DS18B20)


def test_devices_raises_connection_error(patched_owclient):
    (proxy_config, OwClient) = patched_owclient
    proxy_config.raise_on_connect = True
    owc = OwClient()

    with pytest.raises(CouldNotConnectError):
        owc.devices


def test_devices_raises_could_not_load_devices(patched_owclient):
    (proxy_config, OwClient) = patched_owclient
    proxy_config.raise_on_dir = True
    owc = OwClient()

    with pytest.raises(CouldNotLoadDevicesError):
        owc.devices


def test_devices_empty_list(patched_owclient):
    (proxy_config, OwClient) = patched_owclient
    owc = OwClient()

    assert len(owc.devices) == 0


def test_devices_returns_paths(patched_owclient):
    (proxy_config, OwClient) = patched_owclient

    proxy_config.devices = [{
        'path': '1',
        'type': b'UNKNOWN',
        'address': b'address-1',
        'family': b'family-1',
        'crc8': b'crc8-1',
        'id': b'id-1',
        'locator': b'locator-1'
    }, {
        'path': '2',
        'type': b'DS18S20',
        'address': b'address-2',
        'family': b'family-2',
        'crc8': b'crc8-2',
        'id': b'id-2',
        'locator': b'locator-2',
        'temperature': 20
    }, {
        'path': '3',
        'type': b'DS18B20',
        'address': b'address-3',
        'family': b'family-3',
        'crc8': b'crc8-3',
        'id': b'id-3',
        'locator': b'locator-3',
        'temperature': 30
    }]
    owc = OwClient()
    devices = owc.devices

    assert len(devices) == len(proxy_config.devices), \
        f'There must be {len(proxy_config.devices)} devices'

    assert isinstance(devices[0], Device), \
        'An unknown device must result in a Device class instance'

    assert isinstance(devices[1], DS18S20), \
        'A known device must return an instance of his class'

    assert isinstance(devices[2], DS18B20), \
        'A known device must return an instance of his class'


def test_cached_devices_raises_connection_error(patched_owclient):
    (proxy_config, OwClient) = patched_owclient
    proxy_config.raise_on_connect = True
    owc = OwClient(uncached=False)

    with pytest.raises(CouldNotConnectError):
        owc.devices


def test_cached_devices_raises_could_not_load_devices(patched_owclient):
    (proxy_config, OwClient) = patched_owclient
    proxy_config.raise_on_dir = True
    owc = OwClient(uncached=False)

    with pytest.raises(CouldNotLoadDevicesError):
        owc.devices


def test_cached_devices_empty_list(patched_owclient):
    (proxy_config, OwClient) = patched_owclient
    owc = OwClient(uncached=False)

    assert len(owc.devices) == 0


def test_cached_devices_returns_paths(patched_owclient):
    (proxy_config, OwClient) = patched_owclient
    owc = OwClient(uncached=False)

    proxy_config.devices = [{
        'path': '1',
        'type': b'UNKNOWN',
        'address': b'address-1',
        'family': b'family-1',
        'crc8': b'crc8-1',
        'id': b'id-1',
        'locator': b'locator-1'
    }, {
        'path': '2',
        'type': b'DS18S20',
        'address': b'address-2',
        'family': b'family-2',
        'crc8': b'crc8-2',
        'id': b'id-2',
        'locator': b'locator-2',
        'temperature': 20
    }, {
        'path': '3',
        'type': b'DS18B20',
        'address': b'address-3',
        'family': b'family-3',
        'crc8': b'crc8-3',
        'id': b'id-3',
        'locator': b'locator-3',
        'temperature': 30
    }]

    devices_readed = owc.devices
    assert len(devices_readed) == len(proxy_config.devices), \
        f'There must be {len(proxy_config.devices)} devices'

    assert isinstance(devices_readed[0], Device), \
        'An unknown device must result in a Device class instance'

    assert isinstance(devices_readed[1], DS18S20), \
        'A known device must return an instance of his class'

    assert isinstance(devices_readed[2], DS18B20), \
        'A known device must return an instance of his class'
